
function getMetaTagValue(name) {
  var metas = document.getElementsByTagName('meta');
  for (var i = 0; i < metas.length; i++) {
    var meta = metas[i];
    if (meta.getAttribute('name') === name) {
      return meta.getAttribute('content');
    }
  }
  return undefined;
}

function fillOutTimingElements(
    millisSinceServerRenderContainerElementId,
    millisSinceDialogCreateContainerElementId,
    dialogCreationTimingContainerElementId) {
  fillOutMillisSinceServerRenderTimingElement(millisSinceServerRenderContainerElementId);
  fillOutMillisSinceDialogCreateTimingElement(millisSinceDialogCreateContainerElementId, dialogCreationTimingContainerElementId);
}

function fillOutMillisSinceServerRenderTimingElement(elementId) {
  var serverRenderTimespamp = parseInt(getMetaTagValue("server-render-timestamp"));
  var now = new Date();
  var millisSinceServerRender = now.getTime() - serverRenderTimespamp;
  renderTimingElement(elementId, millisSinceServerRender, 1000, 'bad', 'good');
}

function fillOutMillisSinceDialogCreateTimingElement(
    millisSinceDialogCreateContainerElementId, dialogCreationTimingContainerElementId) {
  var now = new Date().getTime();
  AP.dialog.getCustomData(function(customData) {
    var timingContainer = document.getElementById(millisSinceDialogCreateContainerElementId);
    if (timingContainer) {
      if (customData && customData.creationTimestamp) {
        var delayMilliseconds = now - customData.creationTimestamp;
        renderTimingElement(millisSinceDialogCreateContainerElementId, delayMilliseconds, 1000, 'good', 'bad');
      } else {
        $('#' + dialogCreationTimingContainerElementId).hide();
      }
    }
  });
}

function renderTimingElement(elementId, valueMilliseconds, thresholdMilliseconds, lessThanThresholdStyle, moreThanThresholdStyle) {
  var styleClass = valueMilliseconds < thresholdMilliseconds ? lessThanThresholdStyle : moreThanThresholdStyle;
  var displayValue = valueMilliseconds > 1000 ? (valueMilliseconds / 1000).toFixed(0) : valueMilliseconds;
  var units = valueMilliseconds > 1000 ? 'seconds' : 'ms';
  $('#' + elementId).addClass(styleClass);
  $('#' + elementId).html(displayValue + ' ' + units);
}

function fillOutGetContextElement(elementId) {
  var container = document.getElementById(elementId);
  if (AP.context && AP.context.getContext) {
    AP.context.getContext(function(context) {
      fillOutContext(context, container);
    });
  } else {
    container.innerHTML = '<div class="bad">AP.context.getContext() is not available :(</div>';
  }
}

function bindBindRefreshContextClickHandler(buttonId, elementId) {
  var button = document.getElementById(buttonId);
  if (button) {
    button.addEventListener('click', function() {
      AP.context.getContext(function(context) {
        var container = document.getElementById(elementId);
        fillOutContext(context, container);
      });
      button.blur();
    }, false);
  }
}

function fillOutContext(context, container) {
  var contextJson = JSON.stringify(context, null, 2);
  var contextHtml = '<div>Retrieved the following context:</div><pre class="good">' + contextJson + '</pre>';
  container.innerHTML = contextHtml;
}

function fillOutWithTokenElement(elementId) {
  var container = document.getElementById(elementId);
  if (AP.context && AP.context.getToken) {
    AP.context.getToken(function(token) {
      container.innerHTML = `<div>Retrieved the following token:</div><pre class="good wrappable">${token}</pre>`;
    });
  } else {
    container.innerHTML = '<div class="bad">AP.context.getToken() is not available :(</div>';
  }
}

function bindBindRefreshTokenClickHandler(buttonId, elementId) {
  var button = document.getElementById(buttonId);
  if (button) {
    button.addEventListener('click', function() {
      AP.context.getToken(function(token) {
        var container = document.getElementById(elementId);
        container.innerHTML = `<div>Retrieved the following token:</div><pre class="good wrappable">${token}</pre>`;
      });
      button.blur();
    }, false);
  }
}

function bindBindSendTokenClickHandler(buttonId, elementId) {
  var button = document.getElementById(buttonId);
  if (button) {
    button.addEventListener('click', function() {
      AP.context.getToken(function(token) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/handle-context-token?context-token=' + token, true);
        xhr.onload = function () {
          // Request finished. Do processing here.
        };
        xhr.onreadystatechange = function() {
          if (xhr.readyState === 4) {
            var responseObject = JSON.parse(xhr.response);
            var healthClass = responseObject && responseObject.status === 'ok' ? 'good' : 'bad';
            var responseJson = JSON.stringify(responseObject, null, 2);
            var container = document.getElementById(elementId);
            container.innerHTML = `<div>Sent the token to the app server and got back:</div><pre class="${healthClass} wrappable">${responseJson}</pre>`;
          }
        };
        xhr.send(null);
      });
      button.blur();
    }, false);
  }
}

function fillOutCurrentURLElement(elementId) {
  var container = document.getElementById(elementId);
  container.innerHTML = window.location.href;
}

function bindOpenDialogClickHandler(elementId, dialogKey, dialogHeaderText) {
  var element = document.getElementById(elementId);
  if (element) {
    element.addEventListener('click', function() {
      AP.dialog.create({
        key: dialogKey,
        width: '600px',
        height: '300px',
        chrome: true,
        header: "My app's dialog",
        customData: {
          creationTimestamp: new Date().getTime()
        }
      });
    }, false);
  }
}

function getUrlParameter(name) {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  var results = regex.exec(location.search);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

function possiblyHideHideables() {
  if (getUrlParameter('hideHideables')) {
    var hideables = document.getElementsByClassName('hideable');
    for (var i = 0; i < hideables.length; i++) {
      var hideable = hideables[i];
      hideable.style.display = "none";
    }
  }
}
