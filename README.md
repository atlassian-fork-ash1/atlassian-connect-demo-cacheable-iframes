## Introduction

This app demonstrates cacheable app iframes.

## Running

### Running in Confluence

To run this app in Confluence, read the
 [online docs](https://developer.atlassian.com/cloud/confluence/getting-started/).
 
Once the app is running in Confluence, you will be able to see the main guide
at {{confluence-base-url}}/wiki/plugins/servlet/ac/atlassian-connect-demo-cacheable-iframe/cacheable-iframes-guide-page

### Running in Jira

This app can be run in Jira using the same procedure above, except the descriptor URL
is {{localBaseUrl}}/jira-app-descriptor.

Once the app is running in Jira, you will be able to see the main guide
at {{jira-base-url}}/plugins/servlet/ac/atlassian-connect-demo-cacheable-iframe/cacheable-iframes-guide-page


